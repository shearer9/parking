# Parking

This is a Laravel app intended for owners of parking lots and their employees.


## Installation


```bash
# install dependencies
composer install

# create database (ex. parking)

# edit .env
rename .env.example to .env and set right credentials
(DB_DATABASE, DB_USERNAME, DB_PASSWORD)

# set application key for encryption
php artisan key:generate

# fill database
php artisan migrate --seed

# serve file
php artisan serve
```

## Usage

Admin - This is the owner of parking lots and he can create profiles for new employees.

```bash
E-mail: admin@admin.com
Password: 123456
```
Logged users are employees working on parking sectors who are responsible for making reservations.
Employees supervise only the parking sector they are in charge of and have the option of issuing and collecting parking tickets. When issuing a ticket, the employee can easily see the status of the free parking lots and on that basis create a ticket for the user.
The first hour of parking is free



## License
[MIT](https://choosealicense.com/licenses/mit/)