<div class="modal fade" id="modal-{{ $parking->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Parking broj: {{ $parking->broj_parkinga }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if ($parking->isFree())
        Želite li zauzeti parking?
        <br>
         Sektor: {{ $parking->sektor }}
        <br>
        @else
        Želite li naplatiti parking?
        <br>
        Sektor: {{ $parking->sektor }}
        <br>
        Ukupna cijena: {{ $parking->currentReservation()->calculatePrice() }}KM
        @endif
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        <button type="submit" class="btn btn-primary" form_id="parking-{{ $parking->id }}">Potvrdi</button>
      </div>
    </div>
  </div>
</div>