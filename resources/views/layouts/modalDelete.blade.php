<div class="modal fade" id="modalDelete-{{ $parking->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Parking broj: {{ $parking->broj_parkinga }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Želite li izbrisati parking ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        <button type="submit" class="btn btn-primary" form_id="parkingDelete-{{ $parking->id }}">Izbriši</button>
      </div>
    </div>
  </div>
</div>