@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-4">
        <table class="table table-hover table-bordered">
            <caption>Lista svih korisnika</caption>
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id korisnika</th>
              <th scope="col">Email</th>
              <th scope="col">Adresa</th>
              <th scope="col">Naziv parkinga</th>
              <th scope="col">Broj parkinga</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->address }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->parkings()->count() }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
</div>
@endsection
