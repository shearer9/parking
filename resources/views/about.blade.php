@extends('layouts.app')

@section('content')
    <div>
        <section id="home" class="container-fluid d-flex">
            <div class="container d-flex justify-content-center align-items-center">
                <div class="row d-flex flex-column justify-content-center align-items-center">
                    <img class="img-fluid logo" src="{{ asset('img/logo.png') }}">
                    <h1 class="text-center">
                        Sustav za naplatu parkinga
                    </h1>
                </div>
            </div>
        </section>

        <section class="container" id="free_spots">
            <h2 class="mt-4 text-center">Lokacije parkinga</h2>
            <div class="row my-4">
                @foreach($users as $user)
                    <div class="col-2 card text-center">
                        <div class="card-body">
                            <h5 class="card-title">{{ $user->name }}</h5>
                            <p class="card-text">{{ $user->address }}</p>
                            <p class="card-text">Slobodnih mjesta: {{ $user->freeParkings()->count() }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section id="about" class="container">
            <h2 class="text-center">O nama</h2>
            <p class="text-center">Olakšavamo naplatu parkinga na vrlo jednostavan i intuitivan način! Aplikacija namjenjena vlasnicima parking parcela i njihovim uposlenicima.</p>
            <div class="columns row">
                <div class="col-12 col-lg-6 d-flex flex-column align-items-center">
                    <i class="fa fa-3x fa-car about_icon"></i>
                    <p class="text-justify">Ova web aplikacija je namjenjena tvrtkama ili trgovačkim lancima
                        koji posjeduju vlastite parking parcele. Na vrlo jednostavan način se olakšava naplata
                        parkinga za svako pojedinačno parking mjesto. Vlasinicima parkinga je omogućen jednostavan pregled svih radnika, te dodavanje novih radnika u sustav. Radnici, s druge strane, nadgledaju samo onaj sektor parkinga za koji su zaduženi, te imaju mogućnost izdavanja parking tiketa i naplate istih. Pri izdavanju tiketa radnik vrlo lako može vidjeti stanje slobodnih parkinga te na temelju toga korisniku izradti tiket koji će ga voditi do željenog parking mjesta. Isto tako pri naplati tiketa radnicima je omogućen brz uvid u trenutačni dug korisnika i izvršenje naplate za isti.
                    </p>
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column align-items-center text-justify">
                    <i class="fa fa-3x fa-list about_icon"></i>
                    <p class="text-justify">Najvažnije tehnologije koje smo koristili za ovaj projekt su:
                    </p>
                    <ul>
                        <li>HTML5 - Korišten za izradu osnovne strukture stranice.</li>
                        <br/>
                        <li>CSS3 - Korišten za izradu osnovnog dizajna i izgleda stranice.</li>
                        <br/>
                        <li>JavaScript - Korišten za dodavanje funkcionalnosti na stranicu.</li>
                        <br/>
                        <li>PHP - Korišten za kreiranje dinamičke web stranice.</li>
                        <br/>
                        <li>Laravel - MVC framework koji je uvelike pomogao pri brzini izrade i sigurnosti aplikacije.</li>
                        <br/>
                    </ul>
                </div>
                <div class="col-12 col-gl-6 d-flex flex-column align-items-center"><br>
                    <i class="fa fa-3x fa-question-circle about_icon"></i>
                    <h3 class="text-center">
                        Testni podatci<br><br>
                    </h3>
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column align-items-center">
                    <h4>Test login:</h4>
                    <p class="text-justify">
                        E-mail: test@test.com<br>
                        Password: 123456<br>
                    </p>
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column align-items-center">
                    <h4>Admin login:</h4>
                    <p class="text-justify">
                        E-mail: admin@admin.com<br>
                        Password: 123456<br>
                    </p>
                </div>

                 <div class="col-12 col-gl-6 d-flex flex-column align-items-center"><br>
                    <h5 class="text-center">Vizija: <a href="Vizija.docx">Klikni ovdje</a></h4><br>
                    <h5 class="text-center">Korisnička dokumentacija: <a href="Korisnicka dokumentacija.docx">Klikni ovdje</a>
                </div>
            </div>
        </section>
        <p id="madeby">Gloria Jozić i Ivana Rajič, Razvoj Web Aplikacija, FSR Mostar @ 2018.</p>
    </div>

@endsection