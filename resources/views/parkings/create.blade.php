@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                <h1>Napravi parking</h1>
                    <hr>
                        <form method="POST" action="/parkings">
                          {{    csrf_field()  }}
                          <div class="form-group">
                            <label for="broj_parkinga">Broj parkinga</label>
                            <input type="number" class="form-control" id="broj_parkinga" name="broj_parkinga">
                          </div>
                          <div class="form-group">
                            <label for="sektor">Sektor</label>
                            <input type="text" class="form-control" id="sektor" name="sektor">
                          </div>
                          <div class="form-group">
                            <label for="cijena_po_satu">Cijena/h</label>
                            <input type="number" step="0.01" class="form-control" id="cijena_po_satu" name="cijena_po_satu">
                            <small class="form-text text-muted">Cijena parkinga po satu (BAM)</small>
                          </div>
                          <button type="submit" class="btn btn-primary form-group">Potvrdi</button>
                          <div class="form-group">
                          </div>
                        </form>
                        @include('layouts.errors')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
