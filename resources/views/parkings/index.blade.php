@extends('layouts.app')

@section('content')
<div class="container">

	<a class="nav-link btn btn-primary w-25 my-4" href="/parkings/create">Novi parking</a>

	@if (!$takenParkings->isEmpty())
	<h1 class="text-center pt-3">Zauzeti</h1>
	<div class="row">

		@foreach ($takenParkings as $parking)
		<form id="parkingDelete-{{ $parking->id }}" method="POST" action="/parkings/{{$parking->id}}">
		    @csrf
			@method('DELETE')
			@include('layouts.modalDelete')
		</form>
		<form id="parking-{{ $parking->id }}" method="POST" action="/reservations">
			@csrf
			<input name="parking_id" type="hidden" value="{{ $parking->id }}">
			<input name="action" type="hidden" value="2">
		@include('layouts.modal')
		</form>
		<div class="col-2 card text-center" style="width: 18rem;">
		  	<div class="card-body">
			    <h5 class="card-title">{{ $parking->broj_parkinga }}</h5>
			    <p class="card-text">Sektor: {{ $parking->sektor }}</p>
			    <p class="card-text">{{ $parking->reservations->last()->created_at }}</p>
			    <small>{{ $parking->reservations->last()->created_at->diffForHumans() }}</small>
			    <hr>
			    <p class="card-text">{{ $parking->cijena_po_satu}}KM/h </p>
			    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-{{ $parking->id }}">Završi
				</button>
				<div class="row d-flex justify-content-center mt-3">
					<a class="btn btn-outline-secondary btn-sm mr-2" href="/parkings/{{ $parking->id }}/edit" role="button">Uredi</a>
					<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDelete-{{$parking->id }}">Izbriši</button>
				</div>
		  </div>
		</div>
		@endforeach	
	</div>
<hr>
@endif

@if (!$freeParkings->isEmpty())
<h1 class="text-center">Slobodni</h1>
	<div class="row">
		@foreach($freeParkings as $parking)
		<form id="parkingDelete-{{ $parking->id }}" method="POST" action="/parkings/{{$parking->id}}">
	    @csrf
		@method('DELETE')
		@include('layouts.modalDelete')
		</form>
		<form id="parking-{{ $parking->id }}" method="POST" action="/reservations">
			@csrf
			<input name="parking_id" type="hidden" value="{{ $parking->id }}">
			<input name="action" type="hidden" value="1">
			@include('layouts.modal')
			
		</form>
				<div class="col-2 card text-center" style="width: 18rem;">
			  	<div class="card-body">
				    <h5 class="card-title">{{ $parking->broj_parkinga }}</h5>
				    <p class="card-text">Sektor: {{ $parking->sektor }}</p>
				    <hr>
				    <p class="card-text">{{ $parking->cijena_po_satu}}KM/h </p>
				    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-{{ $parking->id }}">Zauzmi
					</button>
					<div class="row d-flex justify-content-center mt-3">
						<a class="btn btn-outline-secondary btn-sm mr-2" href="/parkings/{{ $parking->id }}/edit" role="button">Uredi</a>
						<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDelete-{{$parking->id }}">Izbriši</button>
					</div>
				@include('layouts.modal')
			  </div>
			</div>
			@endforeach
	</div>
@endif
</div>
@endsection