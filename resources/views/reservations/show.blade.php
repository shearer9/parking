@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-2 card text-center" style="width: 18rem;">
  			<div class="card-body">
	    		<h5 class="card-title">Parking br.{{ $reservation->parking->broj_parkinga }}</h5>
	    		<p class="card-text">Sektor {{ $reservation->parking->sektor }}</p>
	   			<p class="card-text">Pocetak rezervacije: {{ $reservation->created_at }}</p>
	   			@if($reservation->vrijeme_do)
	    		<p class="card-text">Kraj rezervacije: {{ $reservation->vrijeme_do }}</p>
	    		<hr>
	    		<p class="card-text">{{ $reservation->ukupna_cijena}}KM/h </p>
	    		@else
	    		<p class="card-text">Nije zavrsena </p>
	    		@endif
			</div>
		</div>
	</div>
</div>
@endsection