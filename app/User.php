<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'address', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function parkings()
    {
        return $this->hasMany(Parking::class);
    }

    public function isAdmin()
    {
        return $this->isAdmin == 1;
    }

    public function freeParkings(){
        $freeParkings = collect();
        foreach ($this->parkings as $parking) {
            if ($parking->isFree()) {
                $freeParkings->push($parking);
            }
        }
        return $freeParkings;
    }

    public function takenParkings(){
        $takenParkings = collect();
        foreach ($this->parkings as $parking) {
            if (!$parking->isFree()) {
                $takenParkings->push($parking);
            }
        }
        return $takenParkings;
    }

    public function addParking(Parking $parking)
    {
        $this->parkings()->save($parking);
    }
}
