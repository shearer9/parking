<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Reservation extends Model
{
    
    protected $guarded = [];

    public function parking()
    {
        return $this->belongsTo(Parking::class);
    }

    public function finish() {
        $this->vrijeme_do = Carbon::now();
        $this->ukupna_cijena = $this->calculatePrice();
        $this->save();
    }

    public function calculatePrice(){
        $diff = $this->created_at->diffInHours(Carbon::now());
        return $diff * $this->parking->cijena_po_satu;
    }
}
