<?php

namespace App\Http\Controllers;

use App\Parking;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('reservations.index');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // Action 1 = create; 2 = finish
        $parking = Parking::find(request('parking_id'));
        if (request('action') == 1) {
            if ($parking->isFree()) {
                $parking->reservations()->create();
            }
        } else {
            $parking->finishReservation();
        }


        return redirect('/parkings');
    }

    public function show(Reservation $reservation)
    {
        return view('reservations.show', compact('reservation'));
    }

    public function edit(Reservation $reservation)
    {
        //
    }

    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    public function destroy(Reservation $reservation)
    {
        //
    }
}
