<?php

namespace App\Http\Controllers;

use App\Parking;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ParkingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (auth()->user()->isAdmin()) {
                return redirect('/');
            }
            return $next($request);
        });
    }
    
    public function index()
    {
        $freeParkings = Auth::user()->freeParkings();
        $takenParkings = Auth::user()->takenParkings();

        return view('parkings.index', compact('takenParkings', 'freeParkings'));
    }

    public function create()
    {
        return view('parkings.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
        'broj_parkinga' => 'required|integer',
        'sektor' => 'required',
        'cijena_po_satu' => 'required|numeric|min:0'
        ]);

        Auth::user()->addParking(
            new Parking(request(['broj_parkinga','sektor','cijena_po_satu']))
        );
        
        return redirect('/parkings');
    }

    public function show(Parking $parking)
    {
        //
    }

    public function edit(Parking $parking)
    {
        if ($parking->user->id !== Auth::user()->id) {
            return redirect('parkings');
        }
        return view('parkings.edit', compact('parking'));
    }

    public function update(Request $request, Parking $parking)
    {
        $this->validate(request(), [
            'broj_parkinga' => 'required|integer',
            'sektor' => 'required',
            'cijena_po_satu' => 'required|numeric|min:0'
        ]);

        $parking->update(request([
            'broj_parkinga',
            'sektor',
            'cijena_po_satu'
        ]));

        return redirect('/parkings');
    }

    public function destroy(Parking $parking)
    {
        $parking->delete();
        return redirect('/parkings');
    }
}
