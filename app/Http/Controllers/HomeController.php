<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['about']);
        $this->middleware('admin')->only('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('isAdmin', 0)->get();
        return view('home', compact('users'));
    }

    public function about()
    {
        $users = User::where('isAdmin', 0)->get();
        return view('about', compact('users'));
    }
}
