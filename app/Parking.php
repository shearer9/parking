<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Parking extends Model
{
    protected $fillable = ['broj_parkinga','sektor','cijena_po_satu'];

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function finishReservation()
    {
        if (!$this->isFree() || $this->currentReservation()) {
            $updated = $this->currentReservation()->finish();
            return $updated;
        }
    }

    public function currentReservation()
    {
        return $this->reservations()->latest()->first();
    }

    public function isFree()
    {
        $activeRes = $this->reservations()->whereNull('vrijeme_do')->get();
        return $activeRes->isEmpty();
    }
}
