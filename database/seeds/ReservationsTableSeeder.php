<?php

use Illuminate\Database\Seeder;

class ReservationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reservations')->delete();
        
        \DB::table('reservations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parking_id' => 1,
                'created_at' => '2019-10-04 17:03:46',
                'updated_at' => '2019-10-04 17:05:10',
                'vrijeme_do' => '2019-10-04 17:05:10',
                'ukupna_cijena' => '0.00',
            ),
            1 => 
            array (
                'id' => 2,
                'parking_id' => 5,
                'created_at' => '2019-10-04 17:04:47',
                'updated_at' => '2019-10-04 17:19:08',
                'vrijeme_do' => '2019-10-04 17:19:08',
                'ukupna_cijena' => '0.00',
            ),
            2 => 
            array (
                'id' => 3,
                'parking_id' => 2,
                'created_at' => '2019-10-04 15:06:03',
                'updated_at' => '2019-10-04 17:07:46',
                'vrijeme_do' => '2019-10-04 17:07:46',
                'ukupna_cijena' => '42.00',
            ),
            3 => 
            array (
                'id' => 5,
                'parking_id' => 3,
                'created_at' => '2019-10-04 17:16:21',
                'updated_at' => '2019-10-04 17:16:34',
                'vrijeme_do' => '2019-10-04 17:16:34',
                'ukupna_cijena' => '0.00',
            ),
            4 => 
            array (
                'id' => 6,
                'parking_id' => 6,
                'created_at' => '2019-10-04 17:16:58',
                'updated_at' => '2019-10-04 17:19:02',
                'vrijeme_do' => '2019-10-04 17:19:02',
                'ukupna_cijena' => '0.00',
            ),
            5 => 
            array (
                'id' => 7,
                'parking_id' => 9,
                'created_at' => '2019-10-04 14:19:33',
                'updated_at' => '2019-10-04 17:20:28',
                'vrijeme_do' => '2019-10-04 17:20:28',
                'ukupna_cijena' => '69.00',
            ),
        ));
        
        
    }
}