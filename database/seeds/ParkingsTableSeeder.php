<?php

use Illuminate\Database\Seeder;

class ParkingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('parkings')->delete();
        
        \DB::table('parkings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'broj_parkinga' => 20,
                'sektor' => 'A',
                'cijena_po_satu' => '23.00',
                'created_at' => '2019-09-10 20:07:07',
                'updated_at' => '2019-09-10 20:07:07',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'broj_parkinga' => 21,
                'sektor' => 'A',
                'cijena_po_satu' => '21.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 2,
                'broj_parkinga' => 22,
                'sektor' => 'A',
                'cijena_po_satu' => '22.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'broj_parkinga' => 23,
                'sektor' => 'A',
                'cijena_po_satu' => '22.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 2,
                'broj_parkinga' => 24,
                'sektor' => 'A',
                'cijena_po_satu' => '21.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 2,
                'broj_parkinga' => 25,
                'sektor' => 'A',
                'cijena_po_satu' => '21.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 2,
                'broj_parkinga' => 26,
                'sektor' => 'A',
                'cijena_po_satu' => '23.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 2,
                'broj_parkinga' => 27,
                'sektor' => 'A',
                'cijena_po_satu' => '23.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 2,
                'broj_parkinga' => 28,
                'sektor' => 'A',
                'cijena_po_satu' => '23.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 2,
                'broj_parkinga' => 29,
                'sektor' => 'A',
                'cijena_po_satu' => '22.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 2,
                'broj_parkinga' => 30,
                'sektor' => 'A',
                'cijena_po_satu' => '22.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
            11 => 
            array (
                'id' => 13,
                'user_id' => 2,
                'broj_parkinga' => 31,
                'sektor' => 'A',
                'cijena_po_satu' => '22.00',
                'created_at' => '2019-10-04 17:02:11',
                'updated_at' => '2019-10-04 17:02:11',
            ),
        ));
        
        
    }
}