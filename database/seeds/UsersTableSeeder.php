<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'admin@admin.com',
                'password' => '$2y$10$LY.Rk8cJaOM8YOGPNIrI9uws7S2YqDo6X782dJUeSf1.UfA/ykhPe',
                'address' => 'Admin',
                'name' => 'Admin',
                'isAdmin' => 1,
                'remember_token' => NULL,
                'created_at' => '2018-08-30 11:21:29',
                'updated_at' => '2018-08-30 11:21:29',
            ),
            1 => 
            array (
                'id' => 2,
                'email' => 'test@test.com',
                'password' => '$2y$10$7vo3vi.c8Qc5qTXKAzhiO.Jq96ouSWSTsJoFY5IjZFkpLi0LyDDdC',
                'address' => 'Test 123',
                'name' => 'Test Test',
                'isAdmin' => 0,
                'remember_token' => NULL,
                'created_at' => '2018-09-03 17:43:43',
                'updated_at' => '2018-09-03 17:43:43',
            ),
        ));
        
        
    }
}